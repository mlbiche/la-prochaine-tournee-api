#!/bin/bash

source venv/bin/activate

black .
pnpm exec prettier --write .
