#!/bin/bash

source venv/bin/activate

uvicorn main:app --port $1 --workers 4
