#!/bin/bash

source venv/bin/activate

flake8 .
black --check .
pnpm exec prettier --check .
