from pydantic.datetime_parse import parse_datetime


def test_get_spendings_with_no_data(client):
    response = client.get("/spendings/")
    assert response.status_code == 200, response.text
    data = response.json()
    assert data == {
        "spendings": [],
        "count": 0,
        "balance": {"mado": 0, "thibaud": 0},
    }


def test_get_spendings_with_data(client, spending1, spending2, spending3):
    response = client.get("/spendings/")
    assert response.status_code == 200, response.text
    data = response.json()
    assert data == {
        "spendings": [
            {
                "id": spending3.id,
                "created_time": parse_datetime(spending3.created_time).isoformat(),
                "description": "🍷 Rosemarie",
                "author": "thibaud",
                "price": 51.8,
                "involved_guys": ["mado", "thibaud"],
            },
            {
                "id": spending2.id,
                "created_time": parse_datetime(spending2.created_time).isoformat(),
                "description": "🍕 Pizza Chez Vincent",
                "author": "mado",
                "price": 26.8,
                "involved_guys": ["mado", "thibaud"],
            },
            {
                "id": spending1.id,
                "created_time": parse_datetime(spending1.created_time).isoformat(),
                "description": "🦋 Anti-mite + Produit vaisselle",
                "author": "thibaud",
                "price": 13.0,
                "involved_guys": ["mado"],
            },
        ],
        "count": 3,
        "balance": {"mado": 13.4, "thibaud": 38.9},
    }


def test_get_spendings_with_data_limit(client, spending1, spending2, spending3):
    response = client.get("/spendings/", params={"limit": 2})
    assert response.status_code == 200, response.text
    data = response.json()
    assert data == {
        "spendings": [
            {
                "id": spending3.id,
                "created_time": parse_datetime(spending3.created_time).isoformat(),
                "description": "🍷 Rosemarie",
                "author": "thibaud",
                "price": 51.8,
                "involved_guys": ["mado", "thibaud"],
            },
            {
                "id": spending2.id,
                "created_time": parse_datetime(spending2.created_time).isoformat(),
                "description": "🍕 Pizza Chez Vincent",
                "author": "mado",
                "price": 26.8,
                "involved_guys": ["mado", "thibaud"],
            },
        ],
        "count": 3,
        "balance": {"mado": 13.4, "thibaud": 38.9},
    }


def test_get_spendings_with_data_limit_offset(client, spending1, spending2, spending3):
    response = client.get("/spendings/", params={"limit": 2, "offset": 1})
    assert response.status_code == 200, response.text
    data = response.json()
    assert data == {
        "spendings": [
            {
                "id": spending2.id,
                "created_time": parse_datetime(spending2.created_time).isoformat(),
                "description": "🍕 Pizza Chez Vincent",
                "author": "mado",
                "price": 26.8,
                "involved_guys": ["mado", "thibaud"],
            },
            {
                "id": spending1.id,
                "created_time": parse_datetime(spending1.created_time).isoformat(),
                "description": "🦋 Anti-mite + Produit vaisselle",
                "author": "thibaud",
                "price": 13.0,
                "involved_guys": ["mado"],
            },
        ],
        "count": 3,
        "balance": {"mado": 13.4, "thibaud": 38.9},
    }


def test_create_spending(client):
    response = client.post(
        "/spendings/",
        json={
            "description": "🦋 Anti-mite + Produit vaisselle",
            "author": "thibaud",
            "price": 13,
            "involved_guys": ["mado"],
        },
    )
    assert response.status_code == 201, response.text
    data = response.json()
    assert "id" in data
    assert "created_time" in data
    assert data["description"] == "🦋 Anti-mite + Produit vaisselle"
    assert data["author"] == "thibaud"
    assert data["price"] == 13
    assert data["involved_guys"] == ["mado"]

    spending_id = data["id"]
    spending_created_time = data["created_time"]
    response = client.get(f"/spendings/{spending_id}")
    assert response.status_code == 200, response.text
    data = response.json()
    assert data == {
        "id": spending_id,
        "created_time": parse_datetime(spending_created_time).isoformat(),
        "description": "🦋 Anti-mite + Produit vaisselle",
        "author": "thibaud",
        "price": 13,
        "involved_guys": ["mado"],
    }


def test_create_spending_with_invalid_author(client):
    response = client.post(
        "/spendings/",
        json={
            "description": "🦋 Anti-mite + Produit vaisselle",
            "author": "thibaude",
            "price": 13,
            "involved_guys": ["mado"],
        },
    )
    assert response.status_code == 422, response.text

    response = client.get("/spendings/")
    assert response.status_code == 200, response.text
    data = response.json()
    assert data == {
        "spendings": [],
        "count": 0,
        "balance": {"mado": 0, "thibaud": 0},
    }


def test_create_spending_without_involved_guys(client):
    response = client.post(
        "/spendings/",
        json={
            "description": "🦋 Anti-mite + Produit vaisselle",
            "author": "thibaud",
            "price": 13,
            "involved_guys": [],
        },
    )
    assert response.status_code == 422, response.text

    response = client.get("/spendings/")
    assert response.status_code == 200, response.text
    data = response.json()
    assert data == {
        "spendings": [],
        "count": 0,
        "balance": {"mado": 0, "thibaud": 0},
    }


def test_create_spending_with_too_much_involved_guys(client):
    response = client.post(
        "/spendings/",
        json={
            "description": "🦋 Anti-mite + Produit vaisselle",
            "author": "thibaud",
            "price": 13,
            "involved_guys": ["thibaud", "mado", "tartanpion"],
        },
    )
    assert response.status_code == 422, response.text

    response = client.get("/spendings/")
    assert response.status_code == 200, response.text
    data = response.json()
    assert data == {
        "spendings": [],
        "count": 0,
        "balance": {"mado": 0, "thibaud": 0},
    }


def test_create_spending_with_invalid_involved_guys(client):
    response = client.post(
        "/spendings/",
        json={
            "description": "🦋 Anti-mite + Produit vaisselle",
            "author": "thibaud",
            "price": 13,
            "involved_guys": ["madou"],
        },
    )
    assert response.status_code == 422, response.text

    response = client.get("/spendings/")
    assert response.status_code == 200, response.text
    data = response.json()
    assert data == {
        "spendings": [],
        "count": 0,
        "balance": {"mado": 0, "thibaud": 0},
    }


def test_create_spending_with_unique_involved_guy_equal_to_author(client):
    response = client.post(
        "/spendings/",
        json={
            "description": "🦋 Anti-mite + Produit vaisselle",
            "author": "thibaud",
            "price": 13,
            "involved_guys": ["thibaud"],
        },
    )
    assert response.status_code == 422, response.text

    response = client.get("/spendings/")
    assert response.status_code == 200, response.text
    data = response.json()
    assert data == {
        "spendings": [],
        "count": 0,
        "balance": {"mado": 0, "thibaud": 0},
    }


def test_create_spending_with_missing_description(client):
    response = client.post(
        "/spendings/",
        json={
            "author": "thibaud",
            "price": 13,
            "involved_guys": ["mado"],
        },
    )
    assert response.status_code == 422, response.text

    response = client.get("/spendings/")
    assert response.status_code == 200, response.text
    data = response.json()
    assert data == {
        "spendings": [],
        "count": 0,
        "balance": {"mado": 0, "thibaud": 0},
    }


def test_create_spending_with_missing_author(client):
    response = client.post(
        "/spendings/",
        json={
            "description": "🦋 Anti-mite + Produit vaisselle",
            "price": 13,
            "involved_guys": ["mado"],
        },
    )
    assert response.status_code == 422, response.text

    response = client.get("/spendings/")
    assert response.status_code == 200, response.text
    data = response.json()
    assert data == {
        "spendings": [],
        "count": 0,
        "balance": {"mado": 0, "thibaud": 0},
    }


def test_create_spending_with_missing_price(client):
    response = client.post(
        "/spendings/",
        json={
            "description": "🦋 Anti-mite + Produit vaisselle",
            "author": "thibaud",
            "involved_guys": ["mado"],
        },
    )
    assert response.status_code == 422, response.text

    response = client.get("/spendings/")
    assert response.status_code == 200, response.text
    data = response.json()
    assert data == {
        "spendings": [],
        "count": 0,
        "balance": {"mado": 0, "thibaud": 0},
    }


def test_create_spending_with_missing_involved_guys(client):
    response = client.post(
        "/spendings/",
        json={
            "description": "🦋 Anti-mite + Produit vaisselle",
            "author": "thibaud",
            "price": 13,
        },
    )
    assert response.status_code == 422, response.text

    response = client.get("/spendings/")
    assert response.status_code == 200, response.text
    data = response.json()
    assert data == {
        "spendings": [],
        "count": 0,
        "balance": {"mado": 0, "thibaud": 0},
    }


def test_get_spending(client, spending1):
    response = client.get(f"/spendings/{spending1.id}")
    assert response.status_code == 200, response.text
    data = response.json()
    assert data == {
        "id": spending1.id,
        "created_time": parse_datetime(spending1.created_time).isoformat(),
        "description": "🦋 Anti-mite + Produit vaisselle",
        "author": "thibaud",
        "price": 13,
        "involved_guys": ["mado"],
    }


def test_get_unknown_spending(client, spending1):
    response = client.get(f"/spendings/{spending1.id + 1}")
    assert response.status_code == 404, response.text


def test_get_spending_with_invalid_id(client, spending1):
    response = client.get("/spendings/foo")
    assert response.status_code == 422, response.text


def test_update_spending(client, spending1):
    response = client.patch(
        f"/spendings/{spending1.id}",
        json={
            "description": "🦋 Anti-mite + Produit vaisselle + Éponges",
            "author": "thibaud",
            "price": 15,
            "involved_guys": ["mado", "thibaud"],
        },
    )
    assert response.status_code == 200, response.text
    data = response.json()
    assert data == {
        "id": spending1.id,
        "created_time": parse_datetime(spending1.created_time).isoformat(),
        "description": "🦋 Anti-mite + Produit vaisselle + Éponges",
        "author": "thibaud",
        "price": 15.0,
        "involved_guys": ["mado", "thibaud"],
    }

    response = client.get(f"/spendings/{spending1.id}")
    assert response.status_code == 200, response.text
    data = response.json()
    assert data == {
        "id": spending1.id,
        "created_time": parse_datetime(spending1.created_time).isoformat(),
        "description": "🦋 Anti-mite + Produit vaisselle + Éponges",
        "author": "thibaud",
        "price": 15.0,
        "involved_guys": ["mado", "thibaud"],
    }


def test_update_unknown_spending(client, spending1):
    response = client.patch(
        f"/spendings/{spending1.id + 1}",
        json={
            "description": "🦋 Anti-mite + Produit vaisselle + Éponges",
            "author": "thibaud",
            "price": 15,
            "involved_guys": ["mado", "thibaud"],
        },
    )
    assert response.status_code == 404, response.text


def test_update_spending_with_invalid_author(client, spending1):
    response = client.patch(
        f"/spendings/{spending1.id}",
        json={
            "description": "🦋 Anti-mite + Produit vaisselle",
            "author": "thibaude",
            "price": 13,
            "involved_guys": ["mado"],
        },
    )
    assert response.status_code == 422, response.text

    response = client.get(f"/spendings/{spending1.id}")
    assert response.status_code == 200, response.text
    data = response.json()
    assert data == {
        "id": spending1.id,
        "created_time": parse_datetime(spending1.created_time).isoformat(),
        "description": "🦋 Anti-mite + Produit vaisselle",
        "author": "thibaud",
        "price": 13.0,
        "involved_guys": ["mado"],
    }


def test_update_spending_without_involved_guys(client, spending1):
    response = client.patch(
        f"/spendings/{spending1.id}",
        json={
            "description": "🦋 Anti-mite + Produit vaisselle",
            "author": "thibaud",
            "price": 13,
            "involved_guys": [],
        },
    )
    assert response.status_code == 422, response.text

    response = client.get(f"/spendings/{spending1.id}")
    assert response.status_code == 200, response.text
    data = response.json()
    assert data == {
        "id": spending1.id,
        "created_time": parse_datetime(spending1.created_time).isoformat(),
        "description": "🦋 Anti-mite + Produit vaisselle",
        "author": "thibaud",
        "price": 13.0,
        "involved_guys": ["mado"],
    }


def test_update_spending_with_too_much_involved_guys(client, spending1):
    response = client.patch(
        f"/spendings/{spending1.id}",
        json={
            "description": "🦋 Anti-mite + Produit vaisselle",
            "author": "thibaud",
            "price": 13,
            "involved_guys": ["thibaud", "mado", "tartanpion"],
        },
    )
    assert response.status_code == 422, response.text

    response = client.get(f"/spendings/{spending1.id}")
    assert response.status_code == 200, response.text
    data = response.json()
    assert data == {
        "id": spending1.id,
        "created_time": parse_datetime(spending1.created_time).isoformat(),
        "description": "🦋 Anti-mite + Produit vaisselle",
        "author": "thibaud",
        "price": 13.0,
        "involved_guys": ["mado"],
    }


def test_update_spending_with_invalid_involved_guys(client, spending1):
    response = client.patch(
        f"/spendings/{spending1.id}",
        json={
            "description": "🦋 Anti-mite + Produit vaisselle",
            "author": "thibaud",
            "price": 13,
            "involved_guys": ["madou"],
        },
    )
    assert response.status_code == 422, response.text

    response = client.get(f"/spendings/{spending1.id}")
    assert response.status_code == 200, response.text
    data = response.json()
    assert data == {
        "id": spending1.id,
        "created_time": parse_datetime(spending1.created_time).isoformat(),
        "description": "🦋 Anti-mite + Produit vaisselle",
        "author": "thibaud",
        "price": 13.0,
        "involved_guys": ["mado"],
    }


def test_update_spending_with_unique_involved_guy_equal_to_author(client, spending1):
    response = client.patch(
        f"/spendings/{spending1.id}",
        json={
            "description": "🦋 Anti-mite + Produit vaisselle",
            "author": "thibaud",
            "price": 13,
            "involved_guys": ["thibaud"],
        },
    )
    assert response.status_code == 422, response.text

    response = client.get(f"/spendings/{spending1.id}")
    assert response.status_code == 200, response.text
    data = response.json()
    assert data == {
        "id": spending1.id,
        "created_time": parse_datetime(spending1.created_time).isoformat(),
        "description": "🦋 Anti-mite + Produit vaisselle",
        "author": "thibaud",
        "price": 13.0,
        "involved_guys": ["mado"],
    }


def test_delete_spending(client, spending1, spending2, spending3):
    response = client.delete(f"/spendings/{spending1.id}")
    assert response.status_code == 200, response.text
    data = response.json()
    assert data == {
        "thibaud": 25.9,
        "mado": 13.4,
    }

    response = client.get(f"/spendings/{spending1.id}")
    assert response.status_code == 404, response.text


def test_delete_unknown_spending(client, spending1):
    response = client.delete(f"/spendings/{spending1.id + 1}")
    assert response.status_code == 404, response.text
