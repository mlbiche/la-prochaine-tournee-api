import time
from fastapi.testclient import TestClient
from main import app, get_db
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from db import Base
import pytest
from db.schema import Schema as DbSpending
from dotenv import dotenv_values

config = dotenv_values(".env")


engine = create_engine(
    config.get("TEST_DATABASE_URL"), connect_args={"check_same_thread": False}
)
TestingSessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)


def override_get_db():
    try:
        db = TestingSessionLocal()
        yield db
    finally:
        db.close()


app.dependency_overrides[get_db] = override_get_db


@pytest.fixture()
def client():
    return TestClient(app)


@pytest.fixture(autouse=True)
def empty_db_before_test(tmpdir):
    Base.metadata.create_all(bind=engine)
    yield
    Base.metadata.drop_all(bind=engine)


@pytest.fixture()
def db_for_fixture():
    db = TestingSessionLocal()
    yield db
    db.close()


@pytest.fixture()
def spending1(db_for_fixture):
    created_time = time.time()
    db_spending = DbSpending(
        created_time=created_time,
        description="🦋 Anti-mite + Produit vaisselle",
        author="thibaud",
        price=13,
        involved_guys="mado",
    )
    db_for_fixture.add(db_spending)
    db_for_fixture.commit()
    db_for_fixture.refresh(db_spending)
    return db_spending


@pytest.fixture()
def spending2(db_for_fixture):
    created_time = time.time()
    db_spending = DbSpending(
        created_time=created_time,
        description="🍕 Pizza Chez Vincent",
        author="mado",
        price=26.8,
        involved_guys="mado,thibaud",
    )
    db_for_fixture.add(db_spending)
    db_for_fixture.commit()
    db_for_fixture.refresh(db_spending)
    return db_spending


@pytest.fixture()
def spending3(db_for_fixture):
    created_time = time.time()
    db_spending = DbSpending(
        created_time=created_time,
        description="🍷 Rosemarie",
        author="thibaud",
        price=51.8,
        involved_guys="mado,thibaud",
    )
    db_for_fixture.add(db_spending)
    db_for_fixture.commit()
    db_for_fixture.refresh(db_spending)
    return db_spending
