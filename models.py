from pydantic import BaseModel, Field, conlist, validator
from db.schema import Schema as DbSpending
from datetime import datetime
from enums import GuyEnum


class Balance(BaseModel):
    """
    Describe own much money each guy has spent
    Example: `{"thibaud": 12.0, "mado": 15.0}` - After all the spendings,
    thibaud has spent 12.0, and mado has spent 15.0
    For common spending, it is considered as if the author would have spent half
    the amount for the other guy.
    """

    thibaud: float
    mado: float

    @staticmethod
    def from_db_balance(db_balance: {"thibaud": float, "mado": float}):
        return Balance(
            thibaud=db_balance["thibaud"],
            mado=db_balance["mado"],
        )


class SpendingBase(BaseModel):
    description: str
    author: GuyEnum
    price: float
    involved_guys: conlist(
        GuyEnum, min_items=1, max_items=2, unique_items=True
    ) = Field(
        description="""
        The guys involved in the spending. It can be another person (that should
        be different from the author) or two persons including the author.
        """,
    )

    @validator("involved_guys")
    def unique_involved_guy_must_be_different_from_author(cls, v, values, **kwargs):
        if len(v) == 1 and "author" in values and v[0] == values["author"]:
            raise ValueError(
                "The author can not be the unique guy involved in the spending."
            )
        return v


class SpendingCreate(SpendingBase):
    pass


class Spending(SpendingBase):
    id: int
    created_time: datetime

    @staticmethod
    def from_db_spending(db_spending: DbSpending):
        return Spending(
            id=db_spending.id,
            created_time=db_spending.created_time,
            description=db_spending.description,
            author=db_spending.author,
            price=db_spending.price,
            involved_guys=db_spending.involved_guys.split(","),
        )


class SpendingList(BaseModel):
    spendings: list[Spending]
    count: int
    balance: Balance
