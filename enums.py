from enum import Enum


class GuyEnum(str, Enum):
    """
    Enumerates authorized guys to author or to be involved in la prochaine
    tournée spendings
    """

    thibaud = "thibaud"
    mado = "mado"
