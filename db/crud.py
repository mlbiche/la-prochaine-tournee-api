import time

from models import SpendingCreate
from sqlalchemy.orm import Session
from sqlalchemy import func, not_

from .schema import Schema as DbSpending
from enums import GuyEnum


def get_spending(db: Session, spending_id: int):
    return db.get(DbSpending, spending_id)


def get_spendings(db: Session, offset: int = 0, limit: int = 100):
    spendings = (
        db.query(DbSpending)
        .order_by(DbSpending.created_time.desc())
        .offset(offset)
        .limit(limit)
        .all()
    )
    count = db.query(DbSpending).count()
    balance = calculate_balance(db)
    return {"spendings": spendings, "count": count, "balance": balance}


def create_spending(db: Session, spending: SpendingCreate):
    created_time = time.time()
    db_spending = DbSpending(
        created_time=created_time,
        description=spending.description,
        author=spending.author,
        price=spending.price,
        involved_guys=",".join(spending.involved_guys),
    )
    db.add(db_spending)
    db.commit()
    db.refresh(db_spending)
    return db_spending


def update_spending(db: Session, spending_id: int, spending: SpendingCreate):
    db_spending = db.get(DbSpending, spending_id)
    if db_spending is None:
        return None
    db_spending.description = spending.description
    db_spending.author = spending.author
    db_spending.price = spending.price
    db_spending.involved_guys = ",".join(spending.involved_guys)
    db.commit()
    db.refresh(db_spending)
    return db_spending


def delete_spending(db: Session, spending_id: int):
    db_spending = db.get(DbSpending, spending_id)
    if db_spending is None:
        return None
    db.delete(db_spending)
    db.commit()
    balance = calculate_balance(db)
    return balance


def calculate_balance(db):
    thibaud_balance = calculate_author_balance(db, GuyEnum.thibaud, GuyEnum.mado)
    mado_balance = calculate_author_balance(db, GuyEnum.mado, GuyEnum.thibaud)
    return {"thibaud": thibaud_balance, "mado": mado_balance}


def calculate_author_balance(db, author: GuyEnum, other_guy: GuyEnum):
    if author == other_guy:
        raise Exception(
            """
            Calculating an author balance with the other guy equals to the
            author
            """
        )
    balance = 0
    paid_for_other_guy = (
        db.query(func.sum(DbSpending.price))
        .filter(
            DbSpending.author == author,
            not_(DbSpending.involved_guys.contains(author)),
        )
        .first()[0]
    )
    if paid_for_other_guy is not None:
        balance += paid_for_other_guy
    paid_for_both = (
        db.query(func.sum(DbSpending.price) / 2)
        .filter(
            DbSpending.author == author,
            DbSpending.involved_guys.contains(other_guy),
            DbSpending.involved_guys.contains(author),
        )
        .first()[0]
    )
    if paid_for_both is not None:
        balance += paid_for_both
    return balance
