from sqlalchemy import Column, Float, Integer, String

from .database import Base


class Schema(Base):
    __tablename__ = "spendings"

    id = Column(Integer, primary_key=True, index=True)
    created_time = Column(Integer)
    description = Column(String)
    author = Column(String)
    price = Column(Float)
    involved_guys = Column(String)
