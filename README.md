# la-prochaine-tournee

_la prochaine tournée_ 🍻 is a tiny Web tool used for tracking common spending to decide who, from 🙋🏼‍♀️ Mado or 🙋🏽‍♂️ Thibaud, will pay the next round.

_la prochaine tournée_ is [live for 🙋🏼‍♀️ Mado or 🙋🏽‍♂️ Thibaud](https://mlbiche.gitlab.io/la-prochaine-tournee/) but you can get the hang on it on the [demo version](https://mlbiche.gitlab.io/la-prochaine-tournee-demo/).

## ⛓ Dependencies

To run _la prochaine tournée_, you need:

- `python3.*` _([download link](https://www.python.org/downloads/) - [after installation](https://packaging.python.org/tutorials/installing-packages/#id12))_
- `pip3.*` _(ensure pip is [installed](https://packaging.python.org/tutorials/installing-packages/#id13) and [up to date](https://packaging.python.org/tutorials/installing-packages/#id14))_
- `sqlite3` _([download link](https://www.sqlite.org/download.html))_

And that's it! The project will install all the Python module dependencies itself on the next step. 🤙

## 🏃 Quick start

In the terminal of your choice, go through the following steps:

- Clone the project to the folder of your choice.
- Install the project: `make install` _→ It creates the Python virtual environment and installs all the needed Python modules._

  **Note:** You only have to run this command the first time.

* Start the project: `make start` _→ It launches the server on 4 workers on th port 8080. You can assign any port you want by setting `PORT`variable (i.e. `make start PORT=8082`)._
* **Or** start the project in developer mode: `make dev` _→ It starts the project with a watcher **but only 1 worker**. The server automatically reloads when you edit files._

You're done! The server is now running on http://localhost:8080/. 🚀

## 🤝 Contributing

To contribute to _la prochaine tournée_, you also need:

- `node.js` _([download link](https://nodejs.org/en/))_. After the installation, check that `node` is properly installed by running in the terminal of your choice `node -v`. The command should display the `node` installed version.
- `pnpm` _([installation guide](https://pnpm.io/installation))_, a performant alternative to `npm`. You can also check it installed successfully by running `pnpm -v`.

Then, run:

```bash
pnpm install
pnpm exec simple-git-hooks
```

This way, your code is linted when you commit it.

## 📚 Documentation

When running the server locally, the documentation is accessible on http://localhost:8080/docs.

# 🥊 Testing

The full API is test using [pytest](https://docs.pytest.org/en/7.2.x/).

Database fixtures are defined in `./tests/conftest.py`. 👉 See more about fixtures in [pytest documentation](https://docs.pytest.org/en/7.2.x/how-to/fixtures.html#how-to-fixtures)

To run tests: `make test` _→ It will stop at the first failing test case._

You can generate code coverage using `make coverage`. The generated code coverage can be browsed by opening in any browser `./htmlcov/index.html`.

## 🚀 Deploying

On your hosting server, run:

```bash
make deploy
```

It copies and enable a service that keeps the API running.

_la-prochaine-tournee-api_ has to be placed in `/srv/la-prochaine-tournee/api` folder.
