PORT=8080

start:
	bin/start.sh $(PORT)

install:
	bin/install.sh

dev:
	bin/dev.sh

check:
	bin/check.sh

format:
	bin/format.sh

test:
	pytest -vvx

coverage:
	coverage run -m pytest && coverage html

deploy:
	bin/deploy.sh
