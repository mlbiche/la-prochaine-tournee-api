from fastapi import Depends, FastAPI, HTTPException, status
from sqlalchemy.orm import Session
from db import Base, SessionLocal, crud, engine
from fastapi.middleware.cors import CORSMiddleware
from models import (
    Spending,
    SpendingCreate,
    SpendingList,
    Balance,
)
from dotenv import dotenv_values

config = dotenv_values(".env")

Base.metadata.create_all(bind=engine)

description = """
_la prochaine tournée_ 🍻 is a tiny Web tool used for tracking common spending
to decide who, from 🙋🏼‍♀️ Mado or 🙋🏽‍♂️ Thibaud, will pay the next round.
"""

app = FastAPI(title="la prochaine tournée", description=description, version="0.0.1")

app.add_middleware(
    CORSMiddleware,
    allow_origins=[config.get("APP_URL")],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


# Dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@app.post(
    "/spendings/",
    response_model=Spending,
    status_code=status.HTTP_201_CREATED,
)
def create_spending(spending: SpendingCreate, db: Session = Depends(get_db)):
    db_spending = crud.create_spending(db, spending)
    return Spending.from_db_spending(db_spending)


@app.get("/spendings/", response_model=SpendingList)
def read_spendings(
    offset: int = 0, limit: int = 100, db: Session = Depends(get_db)
) -> SpendingList:
    db_result = crud.get_spendings(db, offset=offset, limit=limit)
    spendings = [
        Spending.from_db_spending(db_spending) for db_spending in db_result["spendings"]
    ]
    balance = Balance.from_db_balance(db_result["balance"])
    return SpendingList(spendings=spendings, count=db_result["count"], balance=balance)


@app.get(
    "/spendings/{spending_id}",
    response_model=Spending,
    responses={
        404: {
            "content": {"application/json": {}},
        }
    },
)
def read_spending(spending_id: int, db: Session = Depends(get_db)) -> Spending:
    db_spending = crud.get_spending(db, spending_id)
    if db_spending is None:
        raise HTTPException(status_code=404, detail="Spending not found")
    return Spending.from_db_spending(db_spending)


@app.patch(
    "/spendings/{spending_id}",
    response_model=Spending,
    responses={
        404: {
            "content": {"application/json": {}},
        }
    },
)
def update_spending(
    spending_id: int, spending: SpendingCreate, db: Session = Depends(get_db)
) -> Spending:
    db_spending = crud.update_spending(db, spending_id, spending)
    if db_spending is None:
        raise HTTPException(status_code=404, detail="Spending not found")
    return Spending.from_db_spending(db_spending)


@app.delete(
    "/spendings/{spending_id}",
    response_model=Balance,
    responses={
        404: {
            "content": {"application/json": {}},
        }
    },
)
def delete_spending(spending_id: int, db: Session = Depends(get_db)):
    db_balance = crud.delete_spending(db, spending_id)
    if db_balance is None:
        raise HTTPException(status_code=404, detail="Spending not found")
    return Balance.from_db_balance(db_balance)


# NOTE: Uncomment for debugging purpose
# if __name__ == "__main__":
#     uvicorn.run(app, port=8080)
